import torch
import torch.nn as nn
import torch.nn.functional as F

USE_CUDA = True

class SimpleSelfAttention(nn.Module):
    def __init__(self, num_capsules=1, num_routes=310):
        super(SimpleSelfAttention, self).__init__()

        #self.in_channels = in_channels
        self.num_routes = num_routes
        self.num_capsules = num_capsules
        self.alpha = 0.3

        #self.W = nn.Parameter(torch.randn(1, num_routes, num_capsules, out_channels, in_channels)) #1,32*6*6, 10, 10, 8

    def forward(self, x):
        '''
        x: batch, num_words, emb_dim,  (torch, tensor)
        input_masks: batch, num_words  (from padding)
        '''
        batch_size = x.size(0)
        third_dim = x.size(-1)
        #x = torch.stack([x] * self.num_capsules, dim=2).unsqueeze(4)

        #W = torch.cat([self.W] * batch_size, dim=0)
        #u_hat = torch.matmul(W, x)
        u_hat = x.unsqueeze(2).unsqueeze(-1) #batch,310,300 -> batch,310, 1,300, 1

        b_ij = torch.zeros(batch_size, self.num_routes, self.num_capsules, 1) #b, 310, 1, 1
        if USE_CUDA:
            b_ij = b_ij.cuda()

        num_iterations = 2
        cos = nn.CosineSimilarity(dim=3, eps=1e-8)
        for iteration in range(num_iterations):
#             if iteration>0:
#                 b_ij = b_ij * input_masks.unsqueeze(-1).unsqueeze(-1)
            
            c_ij = F.softmax(b_ij, dim=1)
            #c_ij = torch.cat([c_ij] * batch_size, dim=0).unsqueeze(4)#batch, 310, 1, 1, 1
            c_ij = c_ij.unsqueeze(4)

            s_j = (c_ij * u_hat).sum(dim=1, keepdim=True) #batch,310,1,1,1*batch,310,1,300,1 -> batch,310,1,300,1->b,1,1,300,1
            #v_j = self.squash(s_j) #b,1,1,300,1
            v_j = self.squash(s_j)

            if iteration < num_iterations - 1:
                #print ("debug:", u_hat.size())
                a_ij = cos(u_hat, v_j.expand(batch_size, self.num_routes, 1, third_dim, 1))
                b_ij = (1-self.alpha)*b_ij + self.alpha*a_ij#.mean(dim=0, keepdim=True)

        return s_j.squeeze(1).squeeze(-1)

    def squash(self, input_tensor):
        squared_norm = (input_tensor ** 2).sum(-2, keepdim=True)
        output_tensor = squared_norm * input_tensor / ((1. + squared_norm) * torch.sqrt(squared_norm))
        return output_tensor
