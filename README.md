The code implementation for the paper "DeepStyle: User Style Embedding for Authorship Attribution of Short Texts (APWeb'20)".

### Steps to run
1. Install dependencies 

Install pytorch>=1.4(1.4 tested) `pip install torch==1.4`

Install sklearn `pip install sklearn`

Install tqdm `pip install tqdm`

2. Train and test

`python main.py`

if test only:

Comment Line 507 in main.py then `python main.py`

Noth that the number in Line 369 should equal to the number of users.


To cite:
```
@inproceedings{deepstyle20,
    title={DeepStyle: User Style Embedding for Authorship Attribution of Short Texts},
    author={Hu, Zhiqiang and Lee, Roy Ka-Wei and Wang, Lei and Lim, Ee-Peng},
    booktitle={Asia-Pacific Web (APWeb) and Web-Age Information Management (WAIM) Joint International Conference on Web and Big Data},
    year={2020},
    organization={Springer}
}
```
