#!/usr/bin/env python
# coding: utf-8

# ### data_processer

# In[1]:


import os,sys
import random
import numpy as np
from collections import defaultdict
from sklearn.feature_extraction.text import CountVectorizer
from tqdm import tqdm_notebook as tqdm
from sklearn import preprocessing
import json
from aggregator import SimpleSelfAttention
os.environ['CUDA_VISIBLE_DEVICES'] = '2'


# In[2]:


class Data_processer():
    """
    Load preprocessed dataset
    return:
    {
        "train_text": List, input_ids of train text.
        "test_text": List, input_ids of test text.
        "train_label": List, the author idx of the train text.
        "text_label": List, the author idx of the test text.
        "user_text_dict": Dict, key is author, value is the posts of the author
        "user_label_dict": Dict, key is author, value is the label of the author's post
        "train_len": Int, the number of samples in training set
        "test_len": Int, the number of samples in the testing set.
        "emb_num": vocab size
    } 
    """
    def __init__(self, args):
        self.args = args
        self.mode = args.mode
        self.data_path = args.data_path
        self.n_fold = args.n_fold

    def data_preprocesser(self):
        with open(self.data_path, 'r') as f:
            data_info = json.load(f)
        data_info = json.loads(data_info)
        
        return data_info


# In[3]:


class Dataloader():
    """
    "Dataloader" is used to crate data iterators for training and testing

    Input: data_info of char1, char2, word, and pos

    return: Batch{
        "batch_train_text": List, batch of the char2 input_ids
        "batch_train_char1": List, batch of the char1 input_ids
        "batch_train_word": List, batch of the word input_ids
        "batch_train_pos": List, batch of the pos input_ids
        "batch_train_label": List, the author of the samples in the batch
        "batch_size": Int, batch_size
        "batch_num": Int, the idx of current batch
    }
    """
    def __init__(self, args, data_info, data_info_pos, data_info_word, data_info_char1, fold_num):
        self.args = args
        self.n_fold = args.n_fold
        self.batch_size = args.batch_size
        self.data_info = data_info
        self.data_info_pos = data_info_pos
        self.data_info_word = data_info_word
        self.data_info_char1 = data_info_char1
        self.fold_num = fold_num
        
        
    def data_iter_train(self):
        batch_size=self.batch_size
        train_text = self.data_info['train_text']
        train_label = self.data_info['train_label']
        train_pos = self.data_info_pos['train_text']
        train_word = self.data_info_word['train_text']
        train_char1 = self.data_info_char1['train_text']

        batch_num = int(len(train_text)/batch_size)
        if batch_num*batch_size < len(train_text):
            batch_num += 1
        
        user_list = [x for x in range(self.args.output_size)]
        user_num = 100
        post_num = int(batch_size/user_num)
        for batch_i in range(batch_num):
            random.shuffle(user_list)
            user = user_list[:user_num]
           
            batch_train_text = []
            batch_train_label = []
            batch_train_pos = []
            batch_train_word = []
            batch_train_char1 = []
            for u in user:
                batch_start = u*100 + random.randint(0, 99)
                batch_end = min(batch_start+post_num, len(train_text))
            
                batch_train_text += train_text[batch_start: batch_end]
                batch_train_label += train_label[batch_start: batch_end]
                batch_train_pos += train_pos[batch_start: batch_end]
                batch_train_word += train_word[batch_start: batch_end]
                batch_train_char1 += train_char1[batch_start: batch_end]

            batch_elem = {'batch_train_text': batch_train_text, 'batch_train_label': batch_train_label, 'batch_size': batch_size, 'batch_num': batch_num, 'batch_train_pos': batch_train_pos, 'batch_train_word': batch_train_word, 'batch_train_char1': batch_train_char1}
        
            yield batch_elem
        


# ### CNN-n model

# In[4]:


import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.nn import Parameter
import torch.optim as optim
from torch.autograd import Variable
import numpy as np
import time
from itertools import combinations


# In[5]:


class CNN_n(nn.Module):
    """
    The CNN module, defines the model structure of CNN.
    """
    def __init__(self, args, data_info):
        super(CNN_n, self).__init__()
        self.args = args
        
        V = data_info['emb_num']
        D = args.emb_dim
        Ci = 1
        Co = args.kernel_num
        Ks = args.kernel_sizes
        C = args.output_size
        
        self.ngram_emb = nn.Embedding(V, D)
        self.convs = nn.ModuleList([nn.Conv2d(Ci, Co, (K, D)) for K in Ks])
        self.dropout = nn.Dropout(args.dropout)
        self.fc = nn.Linear(len(Ks)*Co, C)
        
    def forward(self, x):
        x = self.ngram_emb(x)
        x = self.dropout(x)
        x = x.unsqueeze(1)
        x = [F.relu(conv(x)).squeeze(3) for conv in self.convs]
        x = [F.max_pool1d(i, i.size(2)).squeeze(2) for i in x]
        x = torch.cat(x, 1)
 #       x = self.dropout(x)
#         predict = self.fc(x)
        return x #predict


# In[6]:


class Coextractor(nn.Module):
    """
    The "Coextractor" module is used to extract post embeddings by aggregating the char1, char2, word, and pos embeddings
    
    Input: char2 input_ids, pos input_ids, word input_ids, char1 input_ids

    return: post embeddings.
    """
    def __init__(self, cnn_n, cnn_n_pos, cnn_n_word, cnn_n_char1):
        super(Coextractor, self).__init__()
        self.cnn_n = cnn_n
        self.cnn_n_pos = cnn_n_pos 
        self.cnn_n_word = cnn_n_word
        self.cnn_n_char1 = cnn_n_char1
        self.aggr = SimpleSelfAttention(1, 4)
        
    def forward(self, text, pos, word, char1):
        text_emb = self.cnn_n(text).unsqueeze(1)
        pos_emb = self.cnn_n_pos(pos).unsqueeze(1)
        word_emb = self.cnn_n_word(word).unsqueeze(1)
        char1_emb = self.cnn_n_char1(char1).unsqueeze(1)
        aggr_emb = self.aggr(torch.cat([text_emb, pos_emb, word_emb, char1_emb], dim=1)).squeeze(1)

        return aggr_emb#text_emb+pos_emb+word_emb


# In[7]:


class Argparse():
    def __init__(self):
        self.mode = 'char2'
        self.emb_dim = 200
        self.kernel_num = 500
        self.output_size = 100
        self.batch_size = 200
        self.kernel_sizes = [3, 4, 5]
        self.n_fold = 10
        self.margin = 0.01
        self.dropout = 0.3
        self.epoch = 1000
        self.lr = 1e-3
        self.checkpoint_path = 'Checkpoint/cnn_triplet_checkpoint_50_users/'
        self.data_path = 'data/dscnn_pretrain_fold0_selector_50_char2'


# In[8]:


class Argparse_pos():
    def __init__(self):
        self.mode = 'pos'
        self.emb_dim = 200
        self.kernel_num = 500
        self.output_size = 100
        self.batch_size = 64
        self.kernel_sizes = [3, 4, 5]
        self.n_fold = 10
        self.margin = 0.01
        self.dropout = 0.3
        self.epoch = 1000
        self.lr = 1e-3
        self.checkpoint_path = ''
        self.data_path = 'data/dscnn_pretrain_fold0_selector_50_pos'


# In[9]:


class Argparse_word():
    def __init__(self):
        self.mode = 'word'
        self.emb_dim = 200
        self.kernel_num = 500
        self.output_size = 100
        self.batch_size = 64
        self.kernel_sizes = [3, 4, 5]
        self.n_fold = 10
        self.margin = 0.01
        self.dropout = 0.3
        self.epoch = 1000
        self.lr = 1e-3
        self.checkpoint_path = ''
        self.data_path = 'data/dscnn_pretrain_fold0_selector_50_word'



class Argparse_char1():
    def __init__(self):
        self.mode = 'char1'
        self.emb_dim = 200
        self.kernel_num = 500
        self.output_size = 100
        self.batch_size = 64
        self.kernel_sizes = [3, 4, 5]
        self.n_fold = 10
        self.margin = 0.01
        self.dropout = 0.3
        self.epoch = 1000
        self.lr = 1e-3
        self.checkpoint_path = ''
        self.data_path = 'data/dscnn_pretrain_fold0_selector_50_char1'
# In[10]:


class OnlineTripletLoss(nn.Module):
    """
    Online Triplets loss
    Takes a batch of embeddings and corresponding labels.
    Triplets are generated using triplet_selector object that take embeddings and targets and return indices of
    triplets
    """

    def __init__(self, margin, triplet_selector):
        super(OnlineTripletLoss, self).__init__()
        self.margin = margin
        self.triplet_selector = triplet_selector
        self.cos = nn.CosineSimilarity(dim=1, eps=1e-8)

    def forward(self, embeddings, target):

        triplets = self.triplet_selector.get_triplets(embeddings, target)
        if triplets.size()[0] == 0:
            return nn.Parameter(torch.FloatTensor([0])), 0

        if embeddings.is_cuda:
            triplets = triplets.cuda()
            
        ap_similarity = self.cos(embeddings[triplets[:, 0]], embeddings[triplets[:, 1]])
        an_similarity = self.cos(embeddings[triplets[:, 0]], embeddings[triplets[:, 2]])
        losses = F.relu(an_similarity - ap_similarity + self.margin)

        return losses.mean(), len(triplets)


# In[11]:


class TripletSelector:
    """
    Implementation should return indices of anchors, positive and negative samples
    return np array of shape [N_triplets x 3]
    """

    def __init__(self):
        pass

    def get_triplets(self, embeddings, labels):
        raise NotImplementedError
        
class AllTripletSelector(TripletSelector):
    """
    Returns all possible triplets
    May be impractical in most cases
    """

    def __init__(self):
        super(AllTripletSelector, self).__init__()

    def get_triplets(self, embeddings, labels):
        labels = labels.cpu().data.numpy()
        triplets = []
        for label in set(labels):
            label_mask = (labels == label)
            label_indices = np.where(label_mask)[0]
            if len(label_indices) < 2:
                continue
            negative_indices = np.where(np.logical_not(label_mask))[0]
            anchor_positives = list(combinations(label_indices, 2))  # All anchor-positive pairs

            # Add all negatives for all positive pairs
            temp_triplets = [[anchor_positive[0], anchor_positive[1], neg_ind] for anchor_positive in anchor_positives
                             for neg_ind in negative_indices]
            triplets += temp_triplets

        return torch.LongTensor(np.array(triplets))


# In[12]:


class Trainer():
    """
    This class defines how to train the deepstyle model.

    Init: 
          data_loader: data iterators, 
          data_info: data_info of char2, 
          data_info_pos: data_info of pos, 
          data_info_word: data_info of word, 
          data_info_char1: data_info of char1, 
          coextractor: the Coextractor class, 
          args: arguments
    """
    def __init__(self, data_loader, data_info, data_info_pos, data_info_word, data_info_char1, coextractor, args, fold_num):
        self.data_loader = data_loader
        self.data_info = data_info
        self.data_info_pos = data_info_pos
        self.data_info_word = data_info_word
        self.data_info_char1 = data_info_char1
        self.coextractor = coextractor
        self.args = args
        self.margin = args.margin
        self.epoch = args.epoch
        self.optim = optim.Adam(self.coextractor.parameters(), lr=self.args.lr)
        self.checkpoint_path = args.checkpoint_path
        self.fold_num = fold_num
        
    def post_vec_output(self, load_para, batch_size):
        """
        this function is used for extracting user embedding for testing

        output: List, [author1_embedding, author2_embedding, ...]
                List, auther label [0, 1, 2, 3, ...]

        """
        if load_para:
            checkpoint = self.checkpoint_path + 'fold%d/pretrain-best.pth'%self.fold_num
            if checkpoint != None:
                model_CKPT = torch.load(checkpoint)
                self.coextractor.load_state_dict(model_CKPT['state_dict'])
        self.coextractor.eval()
        
        train_text = self.data_info['train_text']
        train_label = self.data_info['train_label']
        train_pos = self.data_info_pos['train_text']
        train_word = self.data_info_word['train_text']
        train_char1 = self.data_info_char1['train_text']
        
        batch_num = int(len(train_text)/batch_size)
        if batch_num*batch_size < len(train_text):
            batch_num += 1
        
        post_feature = []
        for batch_i in range(batch_num):
            batch_start = batch_i * batch_size
            batch_end = min((batch_i+1)*batch_size, len(train_text))
            
            batch_train_text = train_text[batch_start: batch_end]
            batch_train_pos = train_pos[batch_start: batch_end]
            batch_train_word = train_word[batch_start: batch_end]
            batch_train_char1 = train_char1[batch_start: batch_end]
            batch_train_text_tensor = torch.LongTensor(batch_train_text).cuda()
            batch_train_pos_tensor = torch.LongTensor(batch_train_pos).cuda()
            batch_train_word_tensor = torch.LongTensor(batch_train_word).cuda()
            batch_train_char1_tensor = torch.LongTensor(batch_train_char1).cuda()
            
            post_vec = list(self.coextractor(batch_train_text_tensor, batch_train_pos_tensor, batch_train_word_tensor, batch_train_char1_tensor).mean(0).unsqueeze(0).cpu().detach().numpy())
            post_feature += post_vec
        np.savetxt('feature_visualization/cnn_triplet_post_feature_selector_50_users.txt', post_feature)
        np.savetxt('feature_visualization/cnn_triplet_post_label_selector_50_users.txt', [x for x in range(50)])
        print('output post feature done!')
        return None
    
    def test(self, load_para):
        """
        the test function: we will calculate the similarity between test sample representation and all the user embeddings, and pick the top 1 
        author as  the prediction.
        """
        if load_para:
            checkpoint = self.checkpoint_path + 'fold%d/pretrain-best.pth'%self.fold_num
            if checkpoint != None:
                model_CKPT = torch.load(checkpoint)
                self.coextractor.load_state_dict(model_CKPT['state_dict'])
        self.coextractor.eval()

        prepost_feature = np.loadtxt('feature_visualization/cnn_triplet_post_feature_selector_50_users.txt')
        prepost_label = np.loadtxt('feature_visualization/cnn_triplet_post_label_selector_50_users.txt')
        prepost_feature = torch.FloatTensor(prepost_feature).cuda()

        test_text = self.data_info['test_text']
        test_label = self.data_info['test_label']
        test_pos = self.data_info_pos['test_text']
        test_word = self.data_info_word['test_text']
        test_char1 = self.data_info_char1['test_text']

        correct = 0
        cos = nn.CosineSimilarity(dim=1, eps=1e-8)
        for i in range(len(test_text)):
            text = test_text[i]
            pos = test_pos[i]
            word = test_word[i]
            char1 = test_char1[i]
            text_tensor = torch.LongTensor(text).cuda().unsqueeze(0)
            pos_tensor = torch.LongTensor(pos).cuda().unsqueeze(0)
            word_tensor = torch.LongTensor(word).cuda().unsqueeze(0)
            char1_tensor = torch.LongTensor(char1).cuda().unsqueeze(0)

            text_vec = self.coextractor(text_tensor, pos_tensor, word_tensor, char1_tensor)
            text_vec = text_vec.repeat(prepost_feature.size()[0], 1)

            sim = cos(text_vec, prepost_feature)
            predict = torch.argmax(sim)
            predict_user = prepost_label[predict]
            if predict_user == test_label[i]:
                correct += 1
        print('test_acc:', correct/len(test_label))

        return correct/len(test_label)
        
    def train(self):
        """
        the train fucntion defines how to train the model and the logs during training.
        """
        triplet_selector = AllTripletSelector()
        loss = OnlineTripletLoss(self.margin, triplet_selector)
        best_acc = 0
        for epoch in range(1, self.epoch+1):
            start = time.time()
            train_generator = self.data_loader.data_iter_train()
            epoch_loss = 0.0
            epoch_loss_list = []
            total_len = len(self.data_info['train_text'])
            print('total_len:', total_len)
#             pbar = tqdm(total=total_len)
            for batch_elem in train_generator:
                batch_train_text = batch_elem['batch_train_text']
                batch_train_label = batch_elem['batch_train_label']
                batch_train_pos = batch_elem['batch_train_pos']
                batch_train_word = batch_elem['batch_train_word']
                batch_train_char1 = batch_elem['batch_train_char1']
                batch_num = batch_elem['batch_num']
                batch_size = len(batch_train_label)
                
                self.optim.zero_grad()
                batch_loss = 0.0
                
                batch_train_text_tensor = torch.LongTensor(batch_train_text).cuda()
                batch_train_pos_tensor = torch.LongTensor(batch_train_pos).cuda()
                batch_train_word_tensor = torch.LongTensor(batch_train_word).cuda()
                batch_train_char1_tensor = torch.LongTensor(batch_train_char1).cuda()
                batch_train_label = torch.LongTensor(batch_train_label).cuda()
                batch_text_emb = self.coextractor(batch_train_text_tensor, batch_train_pos_tensor, batch_train_word_tensor, batch_train_char1_tensor)
                batch_loss, triplet_len = loss(batch_text_emb, batch_train_label)
                
                batch_loss.backward()
                self.optim.step()
                
                epoch_loss += (batch_loss.item()/batch_num)
#                 pbar.update(batch_size)
            
#             pbar.close()
            epoch_loss_list.append(epoch_loss)
            end = time.time()
            print('epoch:', epoch, 'epoch_loss:', epoch_loss, 'time:%fh'%((end-start)/3600))
            if epoch % 10 == 0:
                self.post_vec_output(False, 100)
                test_acc = self.test(False)
                print('epoch:', epoch, 'test_acc:', test_acc)
                if test_acc > best_acc:
                    best_acc = test_acc
                    print('Checkpoint update!')
                    if not os.path.exists(self.checkpoint_path):
                        os.mkdir(self.checkpoint_path)
                    if not os.path.exists(self.checkpoint_path+'fold%d/'%self.fold_num):
                        os.mkdir(self.checkpoint_path+'fold%d/'%self.fold_num)
                    lossMIN = min(epoch_loss_list)
                    torch.save({'epoch': epoch, 'state_dict': self.coextractor.state_dict(), 'best_loss': lossMIN,
                                'optimizer': self.optim.state_dict()},
                               self.checkpoint_path + 'fold%d/'%self.fold_num +'pretrain-best.pth')
        return best_acc
                
        


# In[ ]:


def main():
    #char bigram
    args = Argparse()
    data = Data_processer(args)
    data_info = data.data_preprocesser()
    #pos tag
    args_pos = Argparse_pos()
    data_pos = Data_processer(args_pos)
    data_info_pos = data_pos.data_preprocesser()
    #word 
    args_word = Argparse_word()
    data_word = Data_processer(args_word)
    data_info_word = data_word.data_preprocesser()
    #char1
    args_char1 = Argparse_char1()
    data_char1 = Data_processer(args_char1)
    data_info_char1 = data_char1.data_preprocesser()
    
    data_loader = Dataloader(args, data_info, data_info_pos, data_info_word, data_info_char1, 0)
    
    cnn_n = CNN_n(args, data_info)
    cnn_n_pos = CNN_n(args_pos, data_info_pos)
    cnn_n_word = CNN_n(args_word, data_info_word)
    cnn_n_char1 = CNN_n(args_char1, data_info_char1)
    
    coextractor = Coextractor(cnn_n, cnn_n_pos, cnn_n_word, cnn_n_char1).cuda()
    doer = Trainer(data_loader, data_info, data_info_pos, data_info_word, data_info_char1, coextractor, args, 0)
    doer.train()
    test_acc = doer.post_vec_output(True ,100)
    test_acc= doer.test(True)
    
main()


# In[ ]:




