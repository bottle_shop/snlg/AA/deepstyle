import numpy as np
from collections import defaultdict

def u_clu_dic_gen(path, user_num):
    u_clu_dict = defaultdict(dict)
    clu_idx = np.loadtxt(path)
    clu_idx = clu_idx.astype(np.int)
    for i in range(user_num*100):
        user_idx = int(i/100)
        if clu_idx[i] not in u_clu_dict[user_idx]:
            u_clu_dict[user_idx][clu_idx[i]] = []
        u_clu_dict[user_idx][clu_idx[i]].append(i)
    return u_clu_dict
